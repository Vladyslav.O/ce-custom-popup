import React, {useEffect, useState} from 'react';
import {
    render,
    useSettings,
    useShippingAddress,
    Link,
    Modal,
    TextBlock,
} from '@shopify/checkout-ui-extensions-react';

render('Checkout::Dynamic::Render', () => <App/>);

function App() {
    const {
        button_text,
        popup_header,
        popup_body,
        state_on_show,
    } = useSettings();
    const {provinceCode} = useShippingAddress();

    const ModalFragment = (
        <Modal padding title={popup_header}>
            <TextBlock>
                {popup_body}
            </TextBlock>
        </Modal>
    );

    const isFillParams = Boolean(button_text && popup_body && state_on_show);
    const isCurrentState = state_on_show === provinceCode;

    return (<>
        {isFillParams && isCurrentState && <Link overlay={ModalFragment}>
            {button_text}
        </Link>}
    </>);
}
